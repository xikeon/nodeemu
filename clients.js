/**
 * Collection of clients
 */
var Clients = module.exports = exports = {
	collection: [],

	get: function (connectionid) {
		// get client
		for( var index in this.collection )
			if( this.collection[index].connectionid === connectionid )
				return this.collection[index];

		return false;
	},

	add: function (client) {
		this.collection.push(client);
	},

	remove: function (client) {
		var index = this.collection.indexOf( client );
		if( index > -1 )
			this.collection.splice( index, 1 );
	},

	broadcast: function (data) {
		for( i in this.collection )
			this.collection[i].send(data);
	}
};