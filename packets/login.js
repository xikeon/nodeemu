/**
 * Packets for init & login
 */
var Packet = module.require('./../packet'),
	utils = require('./../utils');

utils.debug('[DEBUG] '.green + 'Loaded packet handler: ' + module.filename.split('\\').slice(-1)[0].cyan);

Packet.on( 'CN', function (client) {
	client.send('DUIH');
});

Packet.on( '_R', function (client) {
	client.send("DA" + "QBHIIIKHJIPAIQAdd-MM-yyyy" + String.fromCharCode(2) + "SAHPB/client" + String.fromCharCode(2) + "QBH" + "IJWVVVSNKQCFUBJASMSLKUUOJCOLJQPNSBIRSVQBRXZQOTGPMNJIHLVJCRRULBLUO" + String.fromCharCode(1));
});

Packet.on( 'CL', function (client, data) {
	utils.debug('[DEBUG] SSO: '.green + data.substring(2)); //SSO

	_username = 'Nodehotel';
	_usermotto = 'Nodehotel is awesome!';
	_char = 'hd-605-30.ch-645-81.lg-695-75.sh-730-74.ha-1014-.ea-1404-1315.ca-1805-75';
	_fuserights = 'fuse_housekeeping_alert' + String.fromCharCode(2) + 'fuse_login' + String.fromCharCode(2) + 'housekeeping_ban' + String.fromCharCode(2) + 'fuse_moderator_access';
	_welcome = 'w00t';
	_connid = 1;

	client.send("@B" + _fuserights); // FuseRights need to be send here
	client.send("DbIH"); // Unknown
	client.send("@C"); // Unknown
	client.send("BK" + _welcome); // Welcome Message (needs to be sent later, after navigator is loaded (BV HKI), but only the first time!)3

	client.send("@L" + utils.encodeVL64(200) + utils.encodeVL64(200) + utils.encodeVL64(600) + "H" + utils.encodeVL64(1) + utils.encodeVL64(2) + "Legosteentje" + String.fromCharCode(2) + "II" + "H" + "hd-605-30.ch-645-81.lg-695-75.sh-730-74.ha-1014-.ea-1404-1315.ca-1805-75" + String.fromCharCode(2) + "H" + "00-00-1990" + "COOL10" + String.fromCharCode(2));
	
});

// Initialize/refresh appearance
Packet.on( '@G', function (client) {
	client.send("@E" + _connid + String.fromCharCode(2) + _username + String.fromCharCode(2) + _char + String.fromCharCode(2) + 'M' + String.fromCharCode(2) + _usermotto + String.fromCharCode(2) + String.fromCharCode(2) + "PCch=s02/53,51,44" + String.fromCharCode(2) + "HI");
});