/**
 * User Moderation.
 */
var Packet = module.require('./../packet'),
	utils = require('./../utils');

utils.debug('[DEBUG] '.green + 'Loaded packet handler: ' + module.filename.split('\\').slice(-1)[0].cyan);

// "PICK_CRYFORHELP": "@p"
Packet.on( '@p', function (client) {
	// client is still alive
});

// "CALL_FOR_HELP": "AV"
Packet.on( '@AV', function (client) {
	// client is still alive
});

// "CHANGECALLCATEGORY": "CF"
Packet.on( '@CF', function (client) {
	// client is still alive
});

// "MESSAGETOCALLER": "CG"
Packet.on( '@CG', function (client) {
	// client is still alive
});

// "MODERATIONACTION": "CH"
Packet.on( '@CH', function (client) {
	// client is still alive
});

// "FOLLOW_CRYFORHELP": "EC"
Packet.on( '@EC', function (client) {
	// client is still alive
});

// "GET_PENDING_CALLS_FOR_HELP": "Cm"
Packet.on( '@Cm', function (client) {
	// client is still alive
});

// "DELETE_PENDING_CALLS_FOR_HELP": "Cn"
Packet.on( '@Cn', function (client) {
	// client is still alive
});

// "BANUSER": "E@"
Packet.on( 'E@', function (client) {
	// client is still alive
});

// "DELETE_PENDING_CALLS_FOR_HELP": "Cn"
Packet.on( 'Cn', function (client) {
	// client is still alive
});