/**
 * Messenger Packets.
 */
var Packet = module.require('./../packet'),
	utils = module.require('./../utils');

utils.debug('[DEBUG] '.green + 'Loaded packet handler: ' + module.filename.split('\\').slice(-1)[0].cyan);

// "FRIENDLIST_UPDATE": "@O"
Packet.on( '@O', function (client) {
	// client is still alive
});

// "FRIENDLIST_REMOVEFRIEND": "@h"
Packet.on( '@h', function (client) {
	// client is still alive
});

// "FRIENDLIST_ACCEPTFRIEND": "@e"
Packet.on( '@e', function (client) {
	// client is still alive
});

// "FRIENDLIST_DECLINEFRIEND": "@f"
Packet.on( '@f', function (client) {
	// client is still alive
});

// "FRIENDLIST_FRIENDREQUEST": "@g"
Packet.on( '@g', function (client) {
	// client is still alive
});

// "FRIENDLIST_GETFRIENDREQUESTS": "Ci"
Packet.on( '@Ci', function (client) {
	// client is still alive
});

// "FOLLOW_FRIEND": "DF"
Packet.on( '@DF', function (client) {
	// client is still alive
});

// "MESSENGER_SENDMSG": "@a"
Packet.on( '@a', function (client, data) {
	var otherClient = client.get(1);

	if( otherClient !== false )
		otherClient.send('BF' + utils.encodeVL64(2) + data.substring(3) + String.fromCharCode(2));
});

// "FRIEND_INVITE": "@b"
Packet.on( '@b', function (client) {
	// client is still alive
});