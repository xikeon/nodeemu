/**
 * Navigator packets
 */
var Packet = module.require('./../packet'),
	utils = require('./../utils');

utils.debug('[DEBUG] '.green + 'Loaded packet handler: ' + module.filename.split('\\').slice(-1)[0].cyan);

// Public rooms
Packet.on( 'BV', function (client, data) {
	var hideFull = utils.decodeVL64(data.substring(2, 3)),
		catID = utils.decodeVL64(data.substring(3));

	// temp
	var type = 0;
	var name = 'Test';
	var parentID = 0;

	utils.debug( '[DEBUG] '.red + hideFull);
	utils.debug( '[DEBUG] '.red + catID);

	// start
	var nav = "C\\" + utils.encodeVL64(hideFull) + utils.encodeVL64(catID) + utils.encodeVL64(type) + name + String.fromCharCode(2) + utils.encodeVL64(0) + utils.encodeVL64(10000) + utils.encodeVL64(parentID);

	// room loop
	// Encoding.encodeVL64(roomIDs[i]) + Encoding.encodeVL64(1) + roomNames[i] + Convert.ToChar(2) + Encoding.encodeVL64(nowVisitors[i]) + Encoding.encodeVL64(maxVisitors[i]) + Encoding.encodeVL64(cataID) + roomDescriptions[i] + Convert.ToChar(2) + Encoding.encodeVL64(roomIDs[i]) + Encoding.encodeVL64(0) + roomCCTs[i] + Convert.ToChar(2) + "HI"
	var roomID = 1;
	var roomName = 'Hallo';
	var roomDescription = 'De eerste test!';
	var nowVisitors = 0;
	var maxVisitors = 100;
	var roomCCT = 'room_theater_halloween';
	nav += utils.encodeVL64(roomID) + utils.encodeVL64(1) + roomName + String.fromCharCode(2) + utils.encodeVL64(nowVisitors) + utils.encodeVL64(maxVisitors) + utils.encodeVL64(catID) + roomDescription + String.fromCharCode(2) + utils.encodeVL64(roomID) + utils.encodeVL64(0) + roomCCT + String.fromCharCode(2) + "HI";

	client.send(nav);
});
// Own room(s)
Packet.on( '@P', function (client) {


	var out = "@P"
	out += '1' + "NDR" + String.fromCharCode(9) + 'ROOMNAME' + String.fromCharCode(9) + 'ROOMOWNER' + String.fromCharCode(9) + utils.encodeVL64(2) + String.fromCharCode(9) + '20' + String.fromCharCode(9) + '40' + String.fromCharCode(9) + '50' + String.fromCharCode(9) + 'roomDescription' + String.fromCharCode(9) + 'roomDescription' + String.fromCharCode(9) + String.fromCharCode(13)
	out += '2' + "NDR" + String.fromCharCode(9) + 'ROOMNAME' + String.fromCharCode(9) + 'ROOMOWNER' + String.fromCharCode(9) + utils.encodeVL64(2) + String.fromCharCode(9) + '20' + String.fromCharCode(9) + '40' + String.fromCharCode(9) + '50' + String.fromCharCode(9) + 'roomDescription' + String.fromCharCode(9) + 'roomDescription' + String.fromCharCode(9) + String.fromCharCode(13)
	out += '3' + "NDR" + String.fromCharCode(9) + 'ROOMNAME' + String.fromCharCode(9) + 'ROOMOWNER' + String.fromCharCode(9) + utils.encodeVL64(2) + String.fromCharCode(9) + '20' + String.fromCharCode(9) + '40' + String.fromCharCode(9) + '50' + String.fromCharCode(9) + 'roomDescription' + String.fromCharCode(9) + 'roomDescription' + String.fromCharCode(9) + String.fromCharCode(13)

	client.send(out);

	// IF WE DONT
	// client.send("@y" + "nodeMUS")
	
});


Packet.on( 'DH', function (client) {

	out = "E_" + utils.encodeVL64(3);
	
	out += utils.encodeVL64(1) + "w00tw00t" + String.fromCharCode(2) + "MikedotJS" + String.fromCharCode(2) + "2" + String.fromCharCode(2) + utils.encodeVL64("10") + utils.encodeVL64("20") + "w00tw00t" + String.fromCharCode(2);
	out += utils.encodeVL64(2) + "w00tw00t" + String.fromCharCode(2) + "MikedotJS" + String.fromCharCode(2) + "2" + String.fromCharCode(2) + utils.encodeVL64("10") + utils.encodeVL64("20") + "w00tw00t" + String.fromCharCode(2);
	out += utils.encodeVL64(3) + "w00tw00t" + String.fromCharCode(2) + "MikedotJS" + String.fromCharCode(2) + "2" + String.fromCharCode(2) + utils.encodeVL64("10") + utils.encodeVL64("20") + "w00tw00t" + String.fromCharCode(2);

	client.send(out);
});


Packet.on( 'BV', function (client) {



});
