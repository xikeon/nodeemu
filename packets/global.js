/**
 * Global packets, such as pings
 */
var Packet = module.require('./../packet'),
	utils = require('./../utils');

utils.debug('[DEBUG] '.green + 'Loaded packet handler: ' + module.filename.split('\\').slice(-1)[0].cyan);

// PONG
Packet.on( 'CD', function (client) {
	// client is still alive
});

// Request for date
Packet.on( '@q', function (client) {
	client.send("Bc" + Date.now());
});