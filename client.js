var utils = require('./utils'),
	Packet = require('./packet'),
	Clients = require('./clients'),
	User = require('./user');
	Packet = require('./packet');

var Client = module.exports = exports = function( socket ) {
	this.connectionid = Clients.collection.length + 1;
	this.connection = socket;
	this.user = new User;

	Clients.add(this);
};

Client.prototype.get = function( connectionid ) {
	return Clients.get(connectionid);
};

Client.prototype.broadcast = function( data ) {
	Clients.broadcast(data);
};

Client.prototype.disconnect = function( data ) {
	Clients.remove(this);
};

Client.prototype.send = function( data ) {
	// Sends Data + CHR1
	data = data.replace( String.fromCharCode(1), String.fromCharCode(160) );

	this.connection.write( data + String.fromCharCode(1) );
	// Console Logging!
	utils.debug('[OUT] HEADER: '.green + data.substring(0, 2) + ' PACKET '.green + data.substring(2));
};

Client.prototype.onData = function( data ) {
	var connectionData = data.toString();
	while (connectionData != "") {
		var packet = data.toString(),
		v = utils.decodeB64(packet.substring(1, 3));
		this.handleData(packet.substring(3, 4 + v));
		connectionData = connectionData.substring(4 + v);
	}
};

Client.prototype.handleData = function( packet ) {
	utils.debug('[IN] HEADER: '.yellow + packet.substring(0, 2) + ' PACKET '.yellow + packet.substring(2) );

	Packet.emit( packet.substring(0, 2), this, packet.substring(2) );
};