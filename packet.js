/**
 * Packet handler
 */
var EventEmitter = require('events').EventEmitter;

module.exports = exports = new EventEmitter();

// load handlers
require('./packets/login');
require('./packets/global');
require('./packets/messenger');
require('./packets/moderation');
require('./packets/navigator');