/**
 * Shutdown
 */
process.on( 'exit', function () {
});

process.on( 'SIGINT', function () {
	process.exit(0);
});

process.on( 'SIGHUP', function () {
	process.exit(0);
});

/**
 * Boot
 */
require('./socket');