var utils = require('./utils'),
	func = process.argv.splice(2, 1).toString(),
	args = process.argv.splice(2);

if( typeof utils[func] !== 'function' ) {
	console.log('invalid function');
	process.exit();
}

for( var index in args ) {
	var match = args[index].match(/^'(.*?)'$/i);
	if( match ) args[index] = match[1];
	else args[index] = parseInt(args[index]);
}

console.log( utils[func].apply(utils[func], args) );