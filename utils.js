/**
 * Encoding
 */
exports.encodeVL64 = function (i) {
	var pos = 0,
		startpos = pos,
		bytes = 1,
		negmask = i >= 0 ? 0 : 4,
		wf = [0, 0, 0, 0, 0, 0, 0];
	i = i < 0 ? i * -1 : i;
	wf[pos] = 64 + (i % 4);
	pos += 1;

	i = Math.floor(i / 4);
	while (i > 0) {
		bytes += 1;
		wf[pos] = 64 + (i % 64);
		pos += 1;
		i = Math.floor(i / 64);
	}
	wf[startpos] = wf[startpos] | (bytes * 8) | negmask;

	var tmp = "";
	for (var j = 0; j < bytes; j += 1) {
		tmp += String.fromCharCode(wf[j]);
	}

	return tmp;
};

exports.decodeVL64 = function (i) {
	var raw = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	for (var j = 0; j < i.length; j += 1) {
		raw[j] = i.charCodeAt(j);
	}
	var pos = 0,
		v = 0,
		negative = (raw[pos] & 4) == 4 ? 1 : 0,
		totalbytes = Math.floor(raw[pos] / 8) % 8;
	v = raw[pos] % 4;
	pos += 1;
	var shiftAmount = 2,
		b = 1;
	while (b < totalbytes) {
		v = v | (raw[pos] % 64) << shiftAmount;
		shiftAmount = 2 + (6 * b);
		pos += 1;
		b += 1;
	}
	v = negative ? v * -1 : v;
	return v;
};

exports.encodeB64 = function (value, length) {
	if( typeof length === 'undefined' && typeof value === 'string' ) {
		value = value.length;
		length = 2;
	}
	var stack = "";
	for (var x = 1; x <= length; x++)
	{
		var offset = 6 * (length - x);
		val = (64 + (value >> offset & 0x3f));
		stack += String.fromCharCode(val);
	}
	return stack;
};

exports.decodeB64 = function (val) {
	val = val.split("");
	var intTot = 0,
		y = 0;
	for (var x = (val.length - 1); x >= 0; x--)
	{
		var intTmp = parseInt(((val[x].charCodeAt(0) - 64)));
		if (y > 0)
		{
			intTmp = intTmp * parseInt((Math.pow(64, y)));
		}
		intTot += intTmp;
		y++;
	}
	return intTot;
};


/**
 * Logging colors
 */
var logStyles = {
	'white'     : ['\x1B[37m', '\x1B[39m'],
	'grey'      : ['\x1B[90m', '\x1B[39m'],
	'black'     : ['\x1B[30m', '\x1B[39m'],
	'blue'      : ['\x1B[34m', '\x1B[39m'],
	'cyan'      : ['\x1B[36m', '\x1B[39m'],
	'green'     : ['\x1B[32m', '\x1B[39m'],
	'magenta'   : ['\x1B[35m', '\x1B[39m'],
	'red'       : ['\x1B[31m', '\x1B[39m'],
	'yellow'    : ['\x1B[33m', '\x1B[39m']
};

for( var style in logStyles ) {
	if( !( typeof logStyles == 'object' ) ) continue;
	Object.defineProperty( String.prototype, style, {
		get: (function (currentStyle) {
			return function() {
				return logStyles[currentStyle][0] + this + logStyles[currentStyle][1];
			};
		})(style)
	});
}


/**
 * Debug
 */
exports.debug = function (msg) {
	if( typeof msg === 'string' ) {
		var charcodes = [0, 1, 2];
		for( var index in charcodes ) {
			msg = msg.replace( new RegExp( String.fromCharCode(charcodes[index]), "g" ), ('[' + charcodes[index].toString() + ']').grey );
		}
	}
	console.log(msg);
}