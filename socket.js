var net = require( 'net' ),
	Client = require( './client' ),
	utils = require('./utils');

net.createServer( function (socket) {
	utils.debug('connected');
	var clientInstance = new Client(socket);
	
	socket.on('connect', function() {
		utils.debug('Client Connected ' + socket.remoteAddress);
		clientInstance.send('@@');
	});

	socket.on('data', function ( data ) {
		clientInstance.onData( data );
	});

	socket.on( 'end', function() {
		utils.debug('server disconnected');
	});

	socket.on( 'close', function() {
		utils.debug('client disconnected');
	});
	
}).listen(30000);